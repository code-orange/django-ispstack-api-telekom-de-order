from django.urls import path
from spyne.protocol.soap import Soap11
from spyne.server.django import DjangoView

from django_ispstack_api_telekom_de_order.django_ispstack_api_telekom_de_order.services.v14.views import (
    CarrierService as CarrierService_v14,
)

urlpatterns = [
    path(
        "v14/in/submit",
        DjangoView.as_view(
            services=[CarrierService_v14],
            tns="http://wholesale.telekom.de/oss/v14/carrier",
            in_protocol=Soap11(validator="soft"),
            out_protocol=Soap11(),
            name="CarrierPort",
        ),
    ),
]
