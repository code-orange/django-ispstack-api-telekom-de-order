from spyne.decorator import rpc
from spyne.model.primitive import AnyDict, Unicode
from spyne.service import ServiceBase


class CarrierService(ServiceBase):
    @rpc(
        Unicode,
        _returns=AnyDict,
        _in_message_name="annehmenMeldungRequest",
        _out_message_name="annehmenMeldungResponse",
    )
    def annehmenMeldung(ctx, annehmenMeldungRequest):
        return {"successful": "true"}
