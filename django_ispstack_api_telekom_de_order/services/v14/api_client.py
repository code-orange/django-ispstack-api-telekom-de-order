import json
from datetime import datetime

import xmltodict
from OpenSSL import crypto
from django.conf import settings

from django_ispstack_access.django_ispstack_access.models import IspAccessCpe


def fetch_cert_details():
    cert_file = settings.TELEKOM_DE_ORDER_CERT_FILE_PUB
    cert = crypto.load_certificate(crypto.FILETYPE_PEM, open(cert_file, "rb").read())
    issuer = cert.get_issuer()
    issued_by = issuer.CN
    serial = cert.get_serial_number()

    return {
        "issuer": issued_by,
        "serial": serial,
    }


def gen_order_xdsl_carrier_and_product_group_change(cpe: IspAccessCpe):
    now = datetime.now()

    data = json.loads(
        open(
            "django_ispstack_api_telekom_de_order/django_ispstack_api_telekom_de_order/services/v14/templates/order_xdsl_carrier_and_product_group_change.json",
            "rb",
        ).read()
    )

    cert_details = fetch_cert_details()

    data["envelope:annehmenAuftragRequest"]["control"]["zeitstempel"] = now.strftime(
        "%Y-%m-%dT%H:%M:%S.000+02:00"
    )

    data["envelope:annehmenAuftragRequest"]["control"]["signaturId"] = {
        "issuer": cert_details["issuer"],
        "serial": cert_details["serial"],
    }

    xml_data = xmltodict.unparse(data, pretty=True)

    return xml_data


def gen_order_xdsl_carrier_and_product_group_change_confirmation(cpe: IspAccessCpe):
    now = datetime.now()

    data = json.loads(
        open(
            "django_ispstack_api_telekom_de_order/django_ispstack_api_telekom_de_order/services/v14/templates/order_xdsl_carrier_and_product_group_change_confirmation.json",
            "rb",
        ).read()
    )

    cert_details = fetch_cert_details()

    data["envelope:annehmenMeldungRequest"]["control"]["zeitstempel"] = now.strftime(
        "%Y-%m-%dT%H:%M:%S.000+02:00"
    )

    data["envelope:annehmenMeldungRequest"]["control"]["signaturId"] = {
        "issuer": cert_details["issuer"],
        "serial": cert_details["serial"],
    }

    xml_data = xmltodict.unparse(data, pretty=True)

    return xml_data


def gen_order_xdsl_carrier_change(cpe: IspAccessCpe):
    now = datetime.now()

    data = json.loads(
        open(
            "django_ispstack_api_telekom_de_order/django_ispstack_api_telekom_de_order/services/v14/templates/order_xdsl_carrier_change.json",
            "rb",
        ).read()
    )

    cert_details = fetch_cert_details()

    data["envelope:annehmenAuftragRequest"]["control"]["zeitstempel"] = now.strftime(
        "%Y-%m-%dT%H:%M:%S.000+02:00"
    )

    data["envelope:annehmenAuftragRequest"]["control"]["signaturId"] = {
        "issuer": cert_details["issuer"],
        "serial": cert_details["serial"],
    }

    xml_data = xmltodict.unparse(data, pretty=True)

    return xml_data


def gen_order_xdsl_characteristics_change(cpe: IspAccessCpe):
    now = datetime.now()

    data = json.loads(
        open(
            "django_ispstack_api_telekom_de_order/django_ispstack_api_telekom_de_order/services/v14/templates/order_xdsl_characteristics_change.json",
            "rb",
        ).read()
    )

    cert_details = fetch_cert_details()

    data["envelope:annehmenAuftragRequest"]["control"]["zeitstempel"] = now.strftime(
        "%Y-%m-%dT%H:%M:%S.000+02:00"
    )

    data["envelope:annehmenAuftragRequest"]["control"]["signaturId"] = {
        "issuer": cert_details["issuer"],
        "serial": cert_details["serial"],
    }

    xml_data = xmltodict.unparse(data, pretty=True)

    return xml_data


def gen_order_xdsl_new(cpe: IspAccessCpe):
    now = datetime.now()

    data = json.loads(
        open(
            "django_ispstack_api_telekom_de_order/django_ispstack_api_telekom_de_order/services/v14/templates/order_xdsl_new.json",
            "rb",
        ).read()
    )

    cert_details = fetch_cert_details()

    data["envelope:annehmenAuftragRequest"]["control"]["zeitstempel"] = now.strftime(
        "%Y-%m-%dT%H:%M:%S.000+02:00"
    )

    data["envelope:annehmenAuftragRequest"]["control"]["signaturId"] = {
        "issuer": cert_details["issuer"],
        "serial": cert_details["serial"],
    }

    xml_data = xmltodict.unparse(data, pretty=True)

    return xml_data


def gen_order_xdsl_product_change(cpe: IspAccessCpe):
    now = datetime.now()

    data = json.loads(
        open(
            "django_ispstack_api_telekom_de_order/django_ispstack_api_telekom_de_order/services/v14/templates/order_xdsl_product_change.json",
            "rb",
        ).read()
    )

    cert_details = fetch_cert_details()

    data["envelope:annehmenAuftragRequest"]["control"]["zeitstempel"] = now.strftime(
        "%Y-%m-%dT%H:%M:%S.000+02:00"
    )

    data["envelope:annehmenAuftragRequest"]["control"]["signaturId"] = {
        "issuer": cert_details["issuer"],
        "serial": cert_details["serial"],
    }

    xml_data = xmltodict.unparse(data, pretty=True)

    return xml_data


def gen_order_xdsl_product_group_change(cpe: IspAccessCpe):
    now = datetime.now()

    data = json.loads(
        open(
            "django_ispstack_api_telekom_de_order/django_ispstack_api_telekom_de_order/services/v14/templates/order_xdsl_product_group_change.json",
            "rb",
        ).read()
    )

    cert_details = fetch_cert_details()

    data["envelope:annehmenAuftragRequest"]["control"]["zeitstempel"] = now.strftime(
        "%Y-%m-%dT%H:%M:%S.000+02:00"
    )

    data["envelope:annehmenAuftragRequest"]["control"]["signaturId"] = {
        "issuer": cert_details["issuer"],
        "serial": cert_details["serial"],
    }

    xml_data = xmltodict.unparse(data, pretty=True)

    return xml_data


def gen_order_xdsl_termination(cpe: IspAccessCpe):
    now = datetime.now()

    data = json.loads(
        open(
            "django_ispstack_api_telekom_de_order/django_ispstack_api_telekom_de_order/services/v14/templates/order_xdsl_termination.json",
            "rb",
        ).read()
    )

    cert_details = fetch_cert_details()

    data["envelope:annehmenAuftragRequest"]["control"]["zeitstempel"] = now.strftime(
        "%Y-%m-%dT%H:%M:%S.000+02:00"
    )

    data["envelope:annehmenAuftragRequest"]["control"]["signaturId"] = {
        "issuer": cert_details["issuer"],
        "serial": cert_details["serial"],
    }

    xml_data = xmltodict.unparse(data, pretty=True)

    return xml_data
