from django.core.management.base import BaseCommand

from django_ispstack_api_telekom_de_order.django_ispstack_api_telekom_de_order.services.v14.api_client import *


class Command(BaseCommand):
    help = "Test run for xml generation"

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        open("data/django_ispstack_api_telekom_de_order/test/VBL.xml", "wt").write(
            gen_order_xdsl_carrier_and_product_group_change(
                IspAccessCpe.objects.get(id=1)
            )
        )

        open("data/django_ispstack_api_telekom_de_order/test/VBL-ABM.xml", "wt").write(
            gen_order_xdsl_carrier_and_product_group_change_confirmation(
                IspAccessCpe.objects.get(id=1)
            )
        )

        open("data/django_ispstack_api_telekom_de_order/test/PV.xml", "wt").write(
            gen_order_xdsl_carrier_change(IspAccessCpe.objects.get(id=1))
        )

        open("data/django_ispstack_api_telekom_de_order/test/LAE.xml", "wt").write(
            gen_order_xdsl_characteristics_change(IspAccessCpe.objects.get(id=1))
        )

        open("data/django_ispstack_api_telekom_de_order/test/NEU.xml", "wt").write(
            gen_order_xdsl_new(IspAccessCpe.objects.get(id=1))
        )

        open("data/django_ispstack_api_telekom_de_order/test/AEN-LMAE.xml", "wt").write(
            gen_order_xdsl_product_change(IspAccessCpe.objects.get(id=1))
        )

        open("data/django_ispstack_api_telekom_de_order/test/PGW.xml", "wt").write(
            gen_order_xdsl_product_group_change(IspAccessCpe.objects.get(id=1))
        )

        open("data/django_ispstack_api_telekom_de_order/test/KUE-KD.xml", "wt").write(
            gen_order_xdsl_termination(IspAccessCpe.objects.get(id=1))
        )

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
